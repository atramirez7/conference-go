import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    content = json.loads(body)
    name = content.get("presenter_name")
    title = content.get("title")
    subject = "Your presentation has been accept"
    email_body = f"{name}  we're happy to tell you that your presentation {title} has been accepted"
    send_mail(
        subject,
        email_body,
        "admin@conference.go",
        [content.get("presenter_email")],
        )



def process_rejection(ch, method, properties, body):
    print("  Received %r" % body)
    content = json.loads(body)
    name = content.get("presenter_name")
    title = content.get("title")
    subject = "Your presentation has been rejected"
    email_body = f"{name}  we're saddened to tell you that your presentation {title} has been accepted"
    send_mail(
        subject,
        email_body,
        "admin@conference.go",
        [content.get("presenter_email")],
        )

    parameters = pika.ConnectionParameters(host='rabbitmq')
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='procress_approvals')
    channel.basic_consume(
        queue='procress_approvals',
        on_message_callback=process_approval,
        auto_ack=True,
        fail_silently=False,
    )

    channel.start_consuming()

    parameters = pika.ConnectionParameters(host='rabbitmq')
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='presentation_rejection')
    channel.basic_consume(
        queue='presentation_rejection',
        on_message_callback=process_rejection,
        auto_ack=True,
        fail_silently=False,
    )

    channel.start_consuming()
