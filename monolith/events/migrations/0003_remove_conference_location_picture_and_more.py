# Generated by Django 4.0.3 on 2023-05-11 21:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_conference_location_picture'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='conference',
            name='location_picture',
        ),
        migrations.AddField(
            model_name='location',
            name='location_picture',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
