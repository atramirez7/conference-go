from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_location_picture(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    parameter = {
        "query": city + " " + state,
        "per_page": 1,
        }
    response = requests.get(url, headers=headers, params=parameter)
    picture = json.loads(response.content)

    picture_url = {
        "picture_url": picture["photos"][0]["src"]["original"],
    }
    try:
        return picture_url
    except (KeyError, IndexError):
        return {"picture_url": None}

def get_weather_data(city, state):
    geocoding_url = "http://api.openweathermap.org/geo/1.0/direct?"
    parameter = {
        "q": city + "," + state + ", US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1
        }
    geocoding_response = requests.get(geocoding_url, params=parameter)
    r = json.loads(geocoding_response.content)
    lat = r[0]["lat"]
    lon = r[0]["lon"]

    url = "https://api.openweathermap.org/data/2.5/weather?"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "unit": "imperial",
    }
    response = requests.get(url, params=params)
    weather_response = json.loads(response.content)

    weather = {
            "temp": weather_response["main"]["temp"],
            "description": weather_response["weather"][0]["description"],
        }
    try:
        return weather
    except (KeyError, IndexError):
        return {"weather": None}
